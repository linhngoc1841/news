// JavaScript Document
$(function() {
	var imgCarousel;
	var $circleRotation = $('.circle_rotation');
	var $circleItem =  $circleRotation.find('a');
	var $slideshow =  $('#slideshow');
	var $boxContent = $slideshow.find('.box_content');                                   
	var $boxHeader= $slideshow.find('.box_header');
	var $contentList;
	
	$(window).load(function(){
		$boxContent.fadeToggle();
		$boxHeader.fadeToggle();
		init(0);
	});

	$('.slideshow_button').click(function() {	
		var i = 0;
		if(this.id == 'next')
			i = imgCarousel.changeImages(-1);
		else
			i = imgCarousel.changeImages(1);
		displayContent(Math.abs(i));
	});
	$('.slideshow_images').mouseenter(function(){
		displayLink('visible');
	});
	$('.slideshow_images').mouseleave(function(){
  		displayLink('hidden');
	});
	
	$circleItem.click(function(){
		var index = getCirIndex(this)
		var deg = 360 - index * 90;
		init(index);
		$circleRotation.css({"transform":"rotate(" + deg + "deg)","transition-duration":"1s"});		
	});
	
	init = function(index){
		displayBox($boxContent,index);
		displayBox($boxHeader,index);
		$imageList = $boxContent.eq(index).find('.slideshow_images');
		$contentList = $boxContent.eq(index).find('.slideshow_content');
		$contentList.find('li').hide();
		imgCarousel = new Carousel($imageList.find('ul'),300,1);
		displayContent(0);
	};
	
	displayBox = function (box,index){
		box.filter(".active").fadeToggle();
		box.filter(".active").removeClass("active");
		box.eq(index).addClass("active");
		box.eq(index).toggle("size",{},500);
	};
	
	displayContent = function (index){
		$contentList.find('li').filter(".active").hide();
		$contentList.find('li').filter(".active").removeClass("active");
		$contentList.find('li').eq(index).addClass("active");
		$contentList.find('li').eq(index).show();
	};
	getCirIndex = function (obj){
		for (var i=0; i< $circleItem.length;i++){
			if(obj.innerHTML == $circleItem.eq(i).text())
				return i;
		}
		return -1
	}

});

var Carousel = function(slider, liW, silderNumber){
	this.$slider = slider;
	this.liW = liW;
	this.sliderW = liW * silderNumber;
	this.liFW = parseInt(liW * (this.$slider.find('li').length));
	this.$slider.css('width', this.liFW + 'px'); 
	this.$slider.css('left', '0px');
}
$.extend(Carousel.prototype,{
	changeImages:function (direction)
	{	  
		var leftX = parseInt(this.$slider.css('left'));
		var leftY = leftX + direction*this.sliderW;	  
		if(leftY < 0 - this.liFW + this.sliderW)
			leftY = 0;	  
		if(leftY > 0)
			leftY = 0 - this.liFW + this.sliderW;
		this.$slider.animate({left: leftY},100);
		return leftY/300;
	}		 
});

function displayLink(status){
	$('.slideshow_button').css('visibility',status);
}

	
		 