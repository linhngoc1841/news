// JavaScript Document
var imagesArray = ["images/newspaper/movies/maleficent-1.jpg","images/newspaper/movies/maleficent-2.jpg","images/newspaper/movies/maleficent-3.jpg","images/newspaper/movies/maleficent-4.jpg","images/newspaper/movies/maleficent-5.jpg"];
var jq = $.noConflict();
var options = {};
options = { to: { width: 1000, height: 70 } };
var imageNumber = 0;
var delay = 5000;
run=setInterval("changeImages(1)", delay);

var $=function(id){
	return document.getElementById(id);
}

function changeImages(direction)
{
	if($('slideshowImages')){
		//  jq('#slideshow').toggle('drop',options,100);
		  imageNumber = imageNumber + direction;
		  
		  if(imageNumber >= imagesArray.length){
				imageNumber = 0;
		  }	
		  
		  if(imageNumber < 0){
				imageNumber = imagesArray.length-1;
		  }
		  $('slideshowImages').src = imagesArray[imageNumber];
		//  	jq('#slideshow').toggle('drop',options,200);
  	}
}

function changeWindowSize(){
	if(window.innerWidth < 1120)
    	$('to_top').style.visibility="hidden";
	else
		$('to_top').style.visibility="visible";
}
function displayLink(status){
	$('prev_link').style.visibility=status;
	$('next_link').style.visibility=status;
}
function linkPosition(){
	$('prev_link').style.top =  $('slideshowImages').height/2 - 33;
	$('next_link').style.top =  $('slideshowImages').height/2 - 33;
}
function prevLink(){
	window.clearInterval(run);
	changeImages(-1);
}

function nextLink(){
	window.clearInterval(run);
	changeImages(1);
}

window.addEventListener("load", function(){
	changeWindowSize();
	linkPosition();
	displayLink('hidden');
})

window.addEventListener("resize", function(){
	changeWindowSize();
});



